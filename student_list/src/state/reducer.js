import { combineReducers } from 'redux'
import studentsReducer from './students/studentsSlice'

const rootreducer = combineReducers({
    students : studentsReducer
})

export default rootreducer