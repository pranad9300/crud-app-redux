const initialState = [
    {
        id: 1,
        fullName: "Pranad Waghmare",
        phoneNumber: "8777945",
        email : "p@email.com"
    },
    
    
    {
        id: 2,
        fullName: "Shreyas Kashikar",
        phoneNumber: "8777946",
        email : "s@email.com"
    }
    
]

const nextStudentId = (students) => {
    const maxId = students.reduce((maxId, student) => Math.max(student.id, maxId), -1)
    return maxId + 1
}

export default function studentsReducer(state = initialState, action) {
    switch (action.type) {
        case 'students/studentAdded':
            return [
                ...state,
                {
                    id: nextStudentId(state),
                    fullName: action.payload.fullName,
                    phoneNumber: action.payload.phoneNumber,
                    email: action.payload.email
                }
            ]
        
        case 'student/studentDelete':
            
            const studentId = action.payload.delete_id
            const newstudents = [...state]
            const index = newstudents.findIndex((student) => student.id === studentId)
            newstudents.splice(index, 1);
            state = newstudents
            return [...state]
            
        case 'student/studentEdited':
            const student = action.payload.student_ 
            const newstudents_ = [...state] 
            const index_ = newstudents_.findIndex((student_) => student_.id === student.id)
            newstudents_[index_] = student
            state = newstudents_
            return [...state]
        default:
            return state
    }
}