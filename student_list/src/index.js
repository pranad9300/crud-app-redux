import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux'
import { store }  from './state/store'
import { BrowserRouter as Router,Routes,Route,Navigate } from "react-router-dom"
import AddStudent from './components/AddStudent'
import EditStudent from './components/EditStudent';


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store} >
      <Router>
        <Routes>
          <Route path='/' element={ <App/> }/>
          <Route path='/addStudent' element={<AddStudent />} />
          <Route path='/studentAdded' element={<Navigate replace to='/' />} />
          <Route path='/editStudent' element={<EditStudent />} />
          <Route path='/studentEdited' element={<Navigate replace to='/' />} />
        </Routes>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
