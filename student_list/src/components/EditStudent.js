import React,{useState} from 'react'
import { useDispatch } from 'react-redux'
import { useLocation,useNavigate } from 'react-router-dom'


function EditStudent() {


    const location = useLocation()

    const student = location.state.student 
    
    const [editStudentForm, setEditStudentForm] = useState({
        id : student.id,
        fullName: student.fullName,
        phoneNumber: student.phoneNumber,
        email : student.email
    })

    const dispatch = useDispatch()
    
    const navigate = useNavigate()

    const handleEditFormChange = (event) => {
        event.preventDefault()
        const fieldName = event.target.getAttribute("name")
        const fieldValue = event.target.value
        const newEditForm = { ...editStudentForm }
        newEditForm[fieldName] = fieldValue
        setEditStudentForm(newEditForm)
        console.log(newEditForm)
    }

    const handleEditFormSubmit = (event) => {
        event.preventDefault()
        dispatch({ type: 'student/studentEdited', payload: { student_ : editStudentForm } })
        navigate('/studentEdited')
    }
    return (
        <div className='container'>
            <div id='heading'>
                <h2 className='p-2'>Edit  {editStudentForm.fullName}</h2>
            </div>
            <div className="form-container">
                <form  onSubmit={handleEditFormSubmit}>
                    <div className="mb-3">
                        <label for="InputName" className="form-label">Name</label>
                        <input name='fullName' type="text" className="form-control" value={editStudentForm.fullName} id="InputName" onChange={handleEditFormChange} />
                    </div>
                    <div className="mb-3">
                        <label for="InputPhoneNumber" className="form-label">Phone</label>
                        <input name='phoneNumber' type="text" className="form-control"
                        value={editStudentForm.phoneNumber}    id="InputPhoneNumber" onChange={handleEditFormChange} />
                    </div>
                    <div className="mb-3">
                        <label for="InputEmail1" className="form-label">Email address</label>
                        <input name='email' type="email" className="form-control" value={editStudentForm.email}  id="InputEmail1" aria-describedby="emailHelp" onChange={handleEditFormChange} />
                    </div>
                    
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )
}

export default EditStudent
