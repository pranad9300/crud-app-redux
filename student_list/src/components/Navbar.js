import React from 'react'

function Navbar() {

    return (

        <nav className="navbar navbar-light bg-light mr-0">
            <div className="container-fluid d-flex justify-content-center">
                <span className="navbar-brand mb-0 h1">Students List</span>    
            </div>    
        </nav>
        
    )

}

export default Navbar
