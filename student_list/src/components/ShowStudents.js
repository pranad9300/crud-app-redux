import React from 'react'
import { useSelector,useDispatch }  from "react-redux"
import { useNavigate } from 'react-router-dom'




function ShowStudents() {

    const students = useSelector((state) => state.students)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const handleDeleteClick = (student_id) => {
        
        const delete_id = student_id
        dispatch({ type: 'student/studentDelete', payload: { delete_id:delete_id }})
        
    }
    
    const handleEditClick = (event, student) => {
        event.preventDefault()
        navigate('/editStudent',{state:{student:student}})
    }




    const student_data = students.map((student) => {
        return (
            <tr>
                <td>{student.id}</td>
                <td>{student.fullName}</td>
                <td>{student.phoneNumber}</td>
                <td>{student.email}</td>
                <td>
                    <button type='button' className="btn btn-success mx-2" onClick={(event)=>handleEditClick(event,student)} >Edit</button>
                    <button type='button' className="btn btn-success mx-2" onClick={()=>handleDeleteClick(student.id)}>Delete</button>
                </td>
            </tr>
        )
    })

    return (
        <div>
            <table className="table table-bordered table-dark">

                <thead>
                    <tr> 
                        <th scope="col">Id</th>
                        <th scope="col">Full Name</th>                  
                        <th scope="col">Phone No</th>
                        <th scope="col">Email</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                
                <tbody>
                    
                    
                      {student_data}  
                        
     
     
                </tbody>
                
            </table>
            
        </div>
    )
}

export default ShowStudents
