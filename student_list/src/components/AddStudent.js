import React,{useState} from 'react'
import './AddStudent.css'
import { useDispatch } from 'react-redux'
import {useNavigate}  from 'react-router-dom'






function AddStudent() {

    

    const [addForm, setAddForm] = useState({
        fullName: "",
        phoneNumber: "",
        email : ""
    })
   
    const dispatch = useDispatch()

    const navigate = useNavigate()

    const handleInputChange = (event) => {
        event.preventDefault()
        
        const fieldName = event.target.getAttribute("name")
        const fieldValue = event.target.value 
        const newFormData = { ...addForm }
        newFormData[fieldName] = fieldValue
        setAddForm(newFormData)
        console.log(newFormData) 
        
   }

    const handleFormSubmit = (event) => {
        event.preventDefault()
        dispatch({ type: 'students/studentAdded', payload: addForm })
        navigate('/studentAdded')
        
   }

    return (
        <div className='container'>
            <div id='heading'>
                <h2 className='p-2'>Add a student.</h2>
            </div>
            <div className="form-container">
                <form onSubmit={handleFormSubmit}>
                    <div className="mb-3">
                        <label for="InputName" className="form-label">Name</label>
                        <input name='fullName' type="text" className="form-control"  id="InputName" onChange={handleInputChange}/>
                    </div>
                    <div className="mb-3">
                        <label for="InputPhoneNumber" className="form-label">Phone</label>
                        <input name='phoneNumber' type="text" className="form-control"  id="InputPhoneNumber" onChange={handleInputChange} />
                    </div>
                    <div className="mb-3">
                        <label for="InputEmail1" className="form-label">Email address</label>
                        <input name='email' type="email" className="form-control" id="InputEmail1" aria-describedby="emailHelp" onChange={handleInputChange}/>
                    </div>
                    
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )
}

export default AddStudent
