import "./App.css";
import Navbar from "./components/Navbar";
import ShowStudents from "./components/ShowStudents";
import React from "react";
import { useNavigate } from "react-router-dom";

function App() {
  const navigate = useNavigate();

  const handleAddStudentClick = (event) => {
    event.preventDefault();
    navigate("/addStudent");
  };

  return (
    <>
      <Navbar />
      <div className="container">
        <div className="bt m-2">
          {/* button  add students */}
          <button
            type="button"
            className="btn btn-primary"
            id="btn_add_student"
            onClick={handleAddStudentClick}
          >
            Add Student
          </button>
        </div>
        <ShowStudents />
      </div>
    </>
  );
}

export default App;
